{

    'name': 'Rest API for Fare.gi',
    'version': '13.1.0',
    'author': 'Arkana, Joenan <joenan@arkana.co.id>',
    'license': 'LGPL-3',
    'category': 'Backend',
    'website': 'http://arkana.co.id/',
    'summary': 'Restful Api Service',
    'description': '''

    Requirement :

    1. pip install PyJWT


    ''',
    # 'external_dependencies': {
    #     'python': ['pyjwt'],
    # },
    'depends': [
        'auth_signup',
        'faregi',
    ],
    'data': [
        # 'views.xml',
        # 'web_timeline.xml',
    ],
    'auto_install': True,
    'installable': True,
    'application': True,
}
