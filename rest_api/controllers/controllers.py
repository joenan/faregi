# -*- coding: utf-8 -*-
import json
from json import JSONEncoder
import werkzeug
import werkzeug.wrappers
import base64
import odoo
from odoo import http, fields, SUPERUSER_ID, api, _
from odoo.http import request
from odoo.exceptions import UserError
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.http import content_disposition, dispatch_rpc, \
    serialize_exception as _serialize_exception, Response
import functools
import hashlib
import logging
import os
import ast
from ast import literal_eval
from odoo.tools.float_utils import float_is_zero
import time
from datetime import datetime, timedelta
import requests
import pytz
from dateutil import tz
import math
import re
from passlib.context import CryptContext
from contextlib import closing
_logger = logging.getLogger(__name__)


class ControllerREST(http.Controller):
    def check_session_id(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            if not request.session.uid:
                return self.error_response(401, 'Unauthorized', 'Unauthorized')
            else:
                request.uid = request.session.uid
            # The code, following the decorator
            return func(self, *args, **kwargs)

        return wrapper

    def error_response(self, status, error, error_descrip):
        return {
            'data': {
                'error': error,
                'error_descrip': error_descrip,
            },
            'code': status,
        }

    def _get_image_url(self, model_name=False, rec_id=False, field_name=False):
        if model_name and rec_id and field_name:
            return request.httprequest.host_url + 'web/image?model=' + model_name + '&id=' + \
                str(rec_id) + '&field=' + field_name
        else:
            return False

    @http.route(['/api/trips/<mode>',
                 '/api/trips/<mode>/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_trips(self, *args, mode, rec_id=False, **kwargs):
        jdata = request.jsonrequest
        res = {}
        Trip = request.env['tour.trip']
        if mode == 'book':
            if not jdata:
                return self.error_response(401, "Error", "Please check values")
            Trip.browse(jdata.get('id', False)).book_user_ids = [
                (4, request.env.user.id)]
        else:
            domain = []
            limit = None
            if mode == 'search':
                if rec_id:
                    domain = [('id', '=', rec_id)]
                else:
                    if not jdata:
                        return self.error_response(401, "Error", "Please check values")
                    city_from = jdata.get('city_from', False)
                    city_to = jdata.get('city_to', False)
                    date_from = jdata.get('date_from', False)
                    date_to = jdata.get('date_to', False)
                    # fix date format
                    date_from_df = fields.Date.from_string(date_from)
                    date_to_df = fields.Date.from_string(date_to)
                    date_from = fields.Date.to_string(date_from_df)
                    date_to = fields.Date.to_string(date_to_df)

                    domain = [
                        ('city_from', 'ilike', city_from),
                        ('city_to', 'ilike', city_to),
                        ('date_from', '=', date_from),
                        ('date_to', '=', date_to),
                    ]
                    if jdata.get('transport', False):
                        domain += [('transport', '=', jdata.get('transport'))]
                    if jdata.get('budget_max', False):
                        domain += [('price', '<=', jdata.get('budget_max'))]
                    if jdata.get('budget_min', False):
                        domain += [('price', '>=', jdata.get('budget_min'))]
                    # number = jdata.get('number', False)
                    limit = 1
            try:
                trips = Trip.search(domain, limit=limit)
            except Exception as e:
                return self.error_response(401, "ORM error", e)
            if mode == 'bookmark':
                trips = trips.filtered(lambda x: x.booked)
            res_trips = []
            if trips:
                currency_symbol = request.env.user.company_id.currency_id.symbol
                for trip in trips:
                    res_trips.append({
                        'id': trip.id,
                        'name': trip.name,
                        'image': self._get_image_url('tour.trip', trip.id, 'image'),
                        'duration': trip.duration,
                        'date_from': fields.Date.to_string(trip.date_from),
                        'date_to': fields.Date.to_string(trip.date_to),
                        'city_from': trip.city_from,
                        'city_to': trip.city_to,
                        'booked': trip.booked,
                        'price': currency_symbol + ' ' + str(f"{int(trip.price):,}").replace(",", "."),
                        'itenerary_ids': [{
                            'id': itenerary.id,
                            'name': itenerary.name,
                            'date': fields.Date.to_string(itenerary.date),
                            'duration': itenerary.duration,
                            'hour': '{0:02.0f}:{1:02.0f}'.format(*divmod(itenerary.hour * 60, 60)),
                            'end': itenerary.end,
                            'todo_id': {
                                'id': itenerary.todo_id.id,
                                'name': itenerary.todo_id.name,
                                'image': self._get_image_url('tour.todo', itenerary.todo_id.id, 'image'),
                                'description': itenerary.todo_id.description,
                                'price': currency_symbol + ' ' + str(f"{int(itenerary.todo_id.price):,}").replace(",", "."),
                            } if itenerary.todo_id else {},
                        } for itenerary in trip.itenerary_ids],
                    })
            res['count'] = len(trips)
            res['records'] = res_trips
        return {
            'data': res,
            'code': 200,
        }

    @http.route(['/api/flights/<mode>',
                 '/api/flights/<mode>/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_flights(self, *args, mode, rec_id=False, **kwargs):
        jdata = request.jsonrequest
        res = {}
        Flight = request.env['tour.trip.flight']
        if mode == 'book':
            if not jdata:
                return self.error_response(401, "Error", "Please check values")
            Flight.browse(jdata.get('id', False)).book_user_ids = [
                (4, request.env.user.id)]
        else:
            domain = []
            limit = None
            if mode == 'search':
                if rec_id:
                    domain = [('id', '=', rec_id)]
                else:
                    if not jdata:
                        return self.error_response(401, "Error", "Please check values")
                    city_from = jdata.get('city_from', False)
                    city_to = jdata.get('city_to', False)
                    date_from = jdata.get('date_from', False)
                    date_to = jdata.get('date_to', False)
                    domain = [
                        ('departure_city', 'ilike', city_from),
                        ('arrival_city', 'ilike', city_to),
                        ('departure_datetime', '=', date_from),
                        ('return_arrival_datetime', '=', date_to),
                    ]
                    # number = jdata.get('number', False)
            try:
                flights = Flight.search(domain, limit=limit)
            except Exception as e:
                return self.error_response(401, "ORM error", e)
            if mode == 'bookmark':
                flights = flights.filtered(lambda x: x.booked)
            res_flights = []
            if flights:
                currency_symbol = request.env.user.company_id.currency_id.symbol
                for flight in flights:
                    res_flights.append({
                        'id': flight.id,
                        'code': flight.code,
                        'price': currency_symbol + ' ' + str(f"{int(flight.price):,}").replace(",", "."),
                        'airline_id': {
                            'id': flight.airline_id.id,
                            'name': flight.airline_id.name,
                            'logo': self._get_image_url('flight.airline', flight.airline_id.id, 'logo'),
                        },
                        'class_type': flight.class_type,
                        'departure_city': flight.departure_city,
                        'arrival_city': flight.arrival_city,
                        'departure_datetime': fields.Datetime.to_string(flight.departure_datetime),
                        'arrival_datetime': fields.Datetime.to_string(flight.arrival_datetime),
                        'return_departure_datetime': fields.Datetime.to_string(flight.return_departure_datetime),
                        'return_arrival_datetime': fields.Datetime.to_string(flight.return_arrival_datetime),
                        'round_trip': flight.round_trip,
                    })
            res['count'] = len(flights)
            res['records'] = res_flights
        return {
            'data': res,
            'code': 200,
        }

    @http.route(['/api/trains/<mode>',
                 '/api/trains/<mode>/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_trains(self, *args, mode, rec_id=False, **kwargs):
        jdata = request.jsonrequest
        res = {}
        Train = request.env['tour.trip.train']
        if mode == 'book':
            if not jdata:
                return self.error_response(401, "Error", "Please check values")
            Train.browse(jdata.get('id', False)).book_user_ids = [
                (4, request.env.user.id)]
        else:
            domain = []
            limit = None
            if mode == 'search':
                if rec_id:
                    domain = [('id', '=', rec_id)]
                else:
                    if not jdata:
                        return self.error_response(401, "Error", "Please check values")
                    city_from = jdata.get('city_from', False)
                    city_to = jdata.get('city_to', False)
                    date_from = jdata.get('date_from', False)
                    date_to = jdata.get('date_to', False)
                    domain = [
                        ('departure_city', 'ilike', city_from),
                        ('arrival_city', 'ilike', city_to),
                        ('departure_datetime', '=', date_from),
                        ('return_arrival_datetime', '=', date_to),
                    ]
                    # number = jdata.get('number', False)
            try:
                trains = Train.search(domain, limit=limit)
            except Exception as e:
                return self.error_response(401, "ORM error", e)
            if mode == 'bookmark':
                trains = trains.filtered(lambda x: x.booked)
            res_trains = []
            if trains:
                currency_symbol = request.env.user.company_id.currency_id.symbol
                for train in trains:
                    res_trains.append({
                        'id': train.id,
                        'code': train.code,
                        'train': train.train,
                        # 'image': login,
                        'price': currency_symbol + ' ' + str(f"{int(train.price):,}").replace(",", "."),
                        'class_type': train.class_type,
                        'departure_city': train.departure_city,
                        'arrival_city': train.arrival_city,
                        'departure_datetime': fields.Datetime.to_string(train.departure_datetime),
                        'arrival_datetime': fields.Datetime.to_string(train.arrival_datetime),
                        'return_departure_datetime': fields.Datetime.to_string(train.return_departure_datetime),
                        'return_arrival_datetime': fields.Datetime.to_string(train.return_arrival_datetime),
                        'round_trip': train.round_trip,
                    })
            res['count'] = len(trains)
            res['records'] = res_trains
        return {
            'data': res,
            'code': 200,
        }

    @http.route(['/api/cars/<mode>',
                 '/api/cars/<mode>/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_cars(self, *args, mode, rec_id=False, **kwargs):
        jdata = request.jsonrequest
        res = {}
        Car = request.env['tour.trip.car']
        if mode == 'book':
            if not jdata:
                return self.error_response(401, "Error", "Please check values")
            Car.browse(jdata.get('id', False)).book_user_ids = [
                (4, request.env.user.id)]
        else:
            domain = []
            limit = None
            if mode == 'search':
                if rec_id:
                    domain = [('id', '=', rec_id)]
                else:
                    if not jdata:
                        return self.error_response(401, "Error", "Please check values")
                    location = jdata.get('location', False)
                    domain = [
                        ('location', 'ilike', location),
                    ]
                    # number = jdata.get('number', False)
            try:
                cars = Car.search(domain, limit=limit)
            except Exception as e:
                return self.error_response(401, "ORM error", e)
            if mode == 'bookmark':
                cars = cars.filtered(lambda x: x.booked)
            res_cars = []
            if cars:
                currency_symbol = request.env.user.company_id.currency_id.symbol
                for car in cars:
                    res_cars.append({
                        'id': car.id,
                        'name': car.name,
                        'transmission': car.transmission,
                        'image': self._get_image_url('tour.trip.car', car.id, 'image'),
                        'price': currency_symbol + ' ' + str(f"{int(car.price):,}").replace(",", "."),
                        'passenger': car.passenger,
                        'driver': car.driver,
                        'location': car.location,
                    })
            res['count'] = len(cars)
            res['records'] = res_cars
        return {
            'data': res,
            'code': 200,
        }

    @http.route(['/api/hotels/<mode>',
                 '/api/hotels/<mode>/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_hotels(self, *args, mode, rec_id=False, **kwargs):
        jdata = request.jsonrequest
        res = {}
        Hotel = request.env['tour.trip.hotel']
        if mode == 'book':
            if not jdata:
                return self.error_response(401, "Error", "Please check values")
            Hotel.browse(jdata.get('id', False)).book_user_ids = [
                (4, request.env.user.id)]
        else:
            domain = []
            limit = None
            if mode == 'search':
                if rec_id:
                    domain = [('id', '=', rec_id)]
                else:
                    if not jdata:
                        return self.error_response(401, "Error", "Please check values")
                    location = jdata.get('location', False)
                    domain = [
                        ('location', 'ilike', location),
                    ]
                    # number = jdata.get('number', False)
            try:
                hotels = Hotel.search(domain, limit=limit)
            except Exception as e:
                return self.error_response(401, "ORM error", e)
            if mode == 'bookmark':
                hotels = hotels.filtered(lambda x: x.booked)
            res_hotels = []
            if hotels:
                currency_symbol = request.env.user.company_id.currency_id.symbol
                for hotel in hotels:
                    res_hotels.append({
                        'id': hotel.id,
                        'name': hotel.name,
                        'description': hotel.description,
                        'image_ids': [{
                            'id': image.id,
                            'name': image.name,
                            'image': self._get_image_url('hotel.image', image.id, 'image'),
                            'primary': image.primary,
                        } for image in hotel.image_ids],
                        'price': currency_symbol + ' ' + str(f"{int(hotel.price):,}").replace(",", "."),
                        'location': hotel.location,
                        'rating': hotel.rating,
                    })
            res['count'] = len(hotels)
            res['records'] = res_hotels
        return {
            'data': res,
            'code': 200,
        }

    @http.route(['/api/explores/all',
                 '/api/explores/search/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_explores(self, *args, rec_id=False, **kwargs):
        res = {}
        Explore = request.env['tour.explore']
        domain = []
        limit = None
        if rec_id:
            domain = [('id', '=', rec_id)]
        try:
            explores = Explore.search(domain, limit=limit)
        except Exception as e:
            return self.error_response(401, "ORM error", e)
        res_explores = []
        if explores:
            currency_symbol = request.env.user.company_id.currency_id.symbol
            for explore in explores:
                res_explores.append({
                    'id': explore.id,
                    'name': explore.name,
                    'description': explore.description,
                    'image': self._get_image_url('tour.explore', explore.id, 'image'),
                    'todo_ids': [{
                        'id': todo.id,
                        'name': todo.name,
                        # 'image': todo.Date.to_string(itenerary.date),
                        'price': currency_symbol + ' ' + str(f"{int(todo.price):,}").replace(",", "."),
                    } for todo in explore.todo_ids],
                })
        res['count'] = len(explores)
        res['records'] = res_explores
        return {
            'data': res,
            'code': 200,
        }

    @http.route(['/api/todos/<mode>',
                 '/api/todos/<mode>/<int:rec_id>'], type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_todos(self, *args, mode, rec_id=False, **kwargs):
        jdata = request.jsonrequest
        res = {}
        Todo = request.env['tour.todo']
        if mode == 'book':
            if not jdata:
                return self.error_response(401, "Error", "Please check values")
            Todo.browse(jdata.get('id', False)).book_user_ids = [
                (4, request.env.user.id)]
        else:
            domain = []
            limit = None
            if mode == 'search':
                if rec_id:
                    domain += [('id', '=', rec_id)]
            try:
                todos = Todo.search(domain, limit=limit)
            except Exception as e:
                return self.error_response(401, "ORM error", e)
            if mode == 'bookmark':
                todos = todos.filtered(lambda x: x.booked)
            res_todos = []
            if todos:
                currency_symbol = request.env.user.company_id.currency_id.symbol
                for todo in todos:
                    res_todos.append({
                        'id': todo.id,
                        'name': todo.name,
                        'description': todo.description,
                        'price': currency_symbol + ' ' + str(f"{int(todo.price):,}").replace(",", "."),
                        'image': self._get_image_url('tour.todo', todo.id, 'image'),
                    })
            res['count'] = len(todos)
            res['records'] = res_todos
        return {
            'data': res,
            'code': 200,
        }
