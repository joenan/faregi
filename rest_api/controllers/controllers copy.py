# -*- coding: utf-8 -*-
import json
import werkzeug
import werkzeug.wrappers
import base64
import odoo
from odoo import http, fields, SUPERUSER_ID, api, _
from odoo.http import request
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.http import content_disposition, dispatch_rpc, \
    serialize_exception as _serialize_exception, Response
import functools
import hashlib
import logging
import os
import ast
from ast import literal_eval
from odoo.tools.float_utils import float_is_zero
import time
from datetime import datetime, timedelta
import requests
import pytz
from dateutil import tz
import math
import re
from passlib.context import CryptContext
from contextlib import closing
_logger = logging.getLogger(__name__)


class ControllerREST(http.Controller):

    # TODO: Not used now, but maybe later in v2
    fields_by_model = {
        'res.partner': [
            'name', 'phone', 'image', 'street', 'street2',
            'city', 'state_id', 'zip',
            'country_id', 'birth_date', 'gender', 'mobile', 'email',
            'active', 'display_name', 'customer', 'supplier',
        ],
        'sale.order': [
            'name', 'partner_id', 'order_line', 'state',
            'company_id', 'amount_total', 'date_order', 'validity_date',
            'is_expired', 'create_date', 'confirmation_date', 'user_id',
            'payment_term_id', 'journal_id', 'amount_untaxed', 'amount_tax',
        ],
        'sale.order.line': [
            'name', 'product_id', 'product_uom_qty', 'discount',
            'price_unit', 'price_subtotal', 'price_total', 'order_id', 'tax_id',
        ],
        'purchase.order': [
            'name', 'partner_id', 'order_line', 'state',
            'company_id', 'amount_total', 'date_order', 'create_date', 'amount_untaxed',
            'amount_tax',
        ],
        'purchase.order.line': [
            'name', 'product_id', 'product_qty', 'discount',
            'price_unit', 'price_subtotal', 'price_total', 'order_id', 'taxes_id',
        ],
        'product.product': [
            'name', 'list_price', 'display_name', 'bom_id', 'bom_line_ids',
            'taxes_id', 'supplier_taxes_id', 'description', 'description_purchase',
            'description_sale', 'categ_id', 'volume', 'weight',
            'sale_ok', 'purchase_ok', 'uom_id', 'company_id',
            'barcode', 'default_code', 'image', 'type', 'qty_available', 'product_tmpl_id', 'standard_price', 'purchase_price',
        ],
        'res.users': ['name', 'login', 'partner_id'],
        'res.company': [
            'name', 'street', 'street2', 'city', 'state_id', 'zip',
            'website', 'email',
        ],
        'account.tax': ['name', 'amount'],
        'account.invoice': [
            'name', 'partner_id', 'number', 'date_invoice',
            'user_id', 'date_due', 'origin', 'amount_total_signed',
            'residual_signed', 'state', 'payment_term_id', 'date_invoice', 'invoice_line_ids',
            'amount_untaxed', 'amount_tax', 'amount_total', 'residual',
        ],
        'account.invoice.line': [
            'name', 'product_id', 'quantity', 'price_unit',
            'price_subtotal', 'invoice_line_tax_ids', 'invoice_id',
        ],
        'account.journal': ['id', 'name', 'type', 'default_debit_account_id', 'default_credit_account_id'],
        'product.category': ['id', 'name'],
        'res.country.state': ['id', 'name'],
        'res.country': ['id', 'name'],
        'account.payment.term': ['id', 'name'],
        'stock.picking': [
            'id', 'name', 'partner_id', 'scheduled_date', 'origin',
            'location_id', 'location_dest_id', 'state', 'backorder_id',
            'move_line_ids', 'move_lines',
        ],
        'stock.move.line': [
            'id', 'product_id', 'move_id', 'picking_id', 'product_uom_id',
            'lot_id', 'qty_done', 'product_uom_qty', 'picking_id',
        ],
        'stock.move': [
            'id', 'name', 'product_id', 'date_expected', 'state', 'location_id',
            'location_dest_id', 'quantity_done', 'product_uom_qty',
            'product_uom', 'picking_type_id', 'product_type', 'reserved_availability', 'picking_id',
        ],
        'stock.location': [
            'id', 'name',
        ],
        'mrp.bom': [
            'product_tmpl_id', 'product_qty', 'type', 'bom_line_ids', 'name'
        ],
        'mrp.bom.line': [
            'product_id', 'product_qty', 'bom_id', 'product_uom_id'
        ],
        'product.template': [
            'id', 'name',
        ],
        'mrp.production': [
            'name', 'product_id', 'product_qty', 'bom_id', 'date_planned_start', 'state', 'move_raw_ids', 'move_finished_ids',
        ],
        'account.move': [
            'name', 'date', 'ref', 'journal_id', 'line_ids', 'state'
        ],
        'account.move.line': [
            'account_id', 'partner_id', 'debit', 'credit', 'name', 'balance',
        ],
        'account.account': [
            'id', 'name', 'code', 'display_name'
        ],
        'hr.attendance': [
            'id', 'display_name', 'check_in', 'check_out', 'employee_id', 'worked_hours',
        ],
        'hr.employee': [
            'id', 'name', 'user_id',
        ],
        'project.project': [
            'id', 'name', 'user_id', 'partner_id', 'tasks',
        ],
        'project.task': [
            'id', 'name', 'project_id', 'user_id', 'date_deadline', 'planned_hours', 'stage_id', 'state',
        ],
        'account.analytic.line': [
            'id', 'name', 'user_id', 'task_id', 'date_deadline', 'unit_amount', 'project_id', 'employee_id', 'date',
            'problem', 'plan',
        ],
        'product.uom': [
            'id', 'name',
        ],
    }

    order_by_model = {
    }

    next_state_by_model = {
        # 'sale.order': {
        #     'draft': ('sale'),
        # },
    }

    model_name_by_code = {
        'producttemplates': 'product.template',
        'users': 'res.users',
        'countries': 'res.country',
        'companies': 'res.company',
        'products': 'product.product',
        'uoms': 'product.uom',
        'categories': 'product.category',
        'partners': 'res.partner',
        'sales': 'sale.order',
        'salelines': 'sale.order.line',
        'purchases': 'purchase.order',
        'purchaselines': 'purchase.order.line',
        'journals': 'account.journal',
        'journalentries': 'account.move',
        'journalitems': 'account.move.line',
        'states': 'res.country.state',
        'invoices': 'account.invoice',
        'invoicelines': 'account.invoice.line',
        'paymentterms': 'account.payment.term',
        'pickings': 'stock.picking',
        'moves': 'stock.move',
        'movelines': 'stock.move.line',
        'locations': 'stock.location',
        'boms': 'mrp.bom',
        'bomlines': 'mrp.bom.line',
        'manufactures': 'mrp.production',
        'accounts': 'account.account',
        'employees': 'hr.employee',
        'projects': 'project.project',
        'tasks': 'project.task',
        'timesheet': 'account.analytic.line',
        'attendance': 'hr.attendance',
    }

    def set_tz(self, date_string, tz_from, tz_to):
        res = date_string
        if date_string:
            res = fields.Datetime.from_string(
                date_string).replace(tzinfo=tz.gettz(tz_from))
            res = res.astimezone(tz.gettz(tz_to))
            res = res.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        return res

    def get_search_domain(self, model_name, params):
        Model = request.env[model_name]
        search_domain = []
        for key in params:
            if key in ('limit', 'offset', 'session_id', 'order_by', 'sort'):
                continue
            if key in Model._fields:
                if isinstance(Model._fields[key], fields.Char) or isinstance(Model._fields[key], fields.Text) or isinstance(Model._fields[key], fields.Selection):
                    search_domain.append(key, 'ilike', params[key])
                else:
                    search_domain.append(key, '=', params[key])
        return search_domain

    def search_read_with_params(self, model_name, params, basic_domain, sudo=False):
        Model = request.env[model_name]
        if sudo:
            Model = Model.sudo()
        page = int(params.get('page', 1))
        limit = int(params.get('limit', 100))
        order_by = params.get('order_by', False)
        sort = params.get('sort', False)

        order = False
        if order_by and sort:
            order = order_by + ' ' + sort
        elif not order and params.get('order', False):
            order = params['order']
        elif not order and model_name in self.order_by_model:
            order = self.order_by_model[model_name]
        else:
            order = 'id desc'

        offset = (page - 1) * limit if limit else 0

        search_domain = []
        for key in params:
            if key in ('limit', 'offset', 'session_id', 'order_by', 'sort', 'order'):
                continue
            if key in Model._fields:
                if isinstance(Model._fields[key], fields.Char) or isinstance(Model._fields[key], fields.Text) or isinstance(Model._fields[key], fields.Selection):
                    search_domain.append((key, 'ilike', params[key]))
                else:
                    if isinstance(Model._fields[key], fields.Many2one) or isinstance(Model._fields[key], fields.Integer):
                        search_domain.append((key, '=', int(params[key])))
                    elif isinstance(Model._fields[key], fields.Many2many) or isinstance(Model._fields[key], fields.One2many):
                        search_domain.append((key, 'in', int(params[key])))
                    elif isinstance(Model._fields[key], fields.Float):
                        search_domain.append((key, '=', float(params[key])))
                    elif isinstance(Model._fields[key], fields.Boolean):
                        if params[key].lower() == 'true':
                            search_domain.append((key, '=', True))
                        elif params[key].lower() == 'false':
                            search_domain.append((key, '=', False))
                    else:
                        search_domain.append((key, '=', params[key]))
        search_domain += basic_domain
        res = Model.search_read(search_domain, self.fields_by_model[
                                model_name], offset=offset, limit=limit, order=order)
        count = Model.search_count(search_domain)
        meta = {
            'total_item': count,
            'total_page': math.ceil(count / limit) if limit else 1,
            'current_page': page,
            'total_item_per_page': limit if limit else count
        }
        return res, meta

    def prepare_fields_GET(self, model_name, res, list_deleted_key=None, final=False):
        if list_deleted_key is None:
            list_deleted_key = []
        Model = request.env[model_name].sudo()
        for key in res:
            if isinstance(Model._fields[key], fields.One2many) or isinstance(Model._fields[key], fields.Many2many):
                if Model._fields[key].comodel_name:
                    comodel_name = Model._fields[key].comodel_name
                    res_fields = self.fields_by_model.get(
                        comodel_name, ['id', 'name']).copy()
                    res[key] = request.env[comodel_name].sudo().browse(
                        res[key]).read(res_fields)
                    for res_line in res[key]:
                        res_line = self.prepare_fields_GET(
                            comodel_name, res_line, [], final=True)
            elif isinstance(Model._fields[key], fields.Many2one):
                if isinstance(res[key], tuple):
                    if Model._fields[key].comodel_name:
                        comodel_name = Model._fields[key].comodel_name
                        res_comodel = request.env[
                            comodel_name].sudo().browse(res[key][0])
                        res_fields = self.fields_by_model.get(
                            comodel_name, ['id', 'name']).copy()
                        if final:
                            res_fields = ['id', 'name']
                        if res_comodel:
                            res[key] = res_comodel.read(res_fields)[0]
                            res[key] = self.prepare_fields_GET(
                                comodel_name, res[key], [], final=True)
                if not res[key]:
                    res[key] = {}

            elif isinstance(Model._fields[key], fields.Datetime):
                if res[key]:
                    res[key] = self.set_tz(res[key], 'UTC', 'Asia/Jakarta')
            elif isinstance(Model._fields[key], fields.Char) or isinstance(Model._fields[key], fields.Text) or isinstance(Model._fields[key], fields.Date):
                if not res[key]:
                    res[key] = ""
            elif isinstance(Model._fields[key], fields.Binary):
                if 'image' in key:
                    if not res.get('image', False):
                        res[key] = False
                    else:
                        try:
                            res[key] = request.httprequest.host_url + 'web/image?model=' + model_name + '&id=' + str(
                                res['id']) + '&field=' + key + '&session_id=' + request.env['ir.http'].session_info()['session_id']
                        except Exception as e:
                            res[key] = request.httprequest.host_url + 'web/image?model=' + \
                                model_name + '&id=' + \
                                str(res['id']) + '&field=' + key
                else:
                    list_deleted_key.append(key)
            elif isinstance(Model._fields[key], fields.Selection):
                if res[key] and Model._fields[key].base_field.selection:
                    try:
                        selection_dict = dict(
                            Model._fields[key].base_field.selection)
                        res[key] = {
                            "value": res[key],
                            "name": selection_dict.get(res[key]),
                        }
                    except Exception as e:
                        res[key] = {"value": res[key], "name": res[key]}
                else:
                    res[key] = {"value": "", "name": ""}

        for key in list_deleted_key:
            del res[key]
        return res

    def prepare_fields_POST(self, model_name, res):
        Model = request.env[model_name]
        list_deleted_key = []
        for key in res:
            if key not in Model._fields:
                continue
            if isinstance(Model._fields[key], fields.Binary):
                # list_deleted_key.append(key)
                print(key)
            elif isinstance(Model._fields[key], fields.Many2one):
                if isinstance(res[key], dict) and "id" in res[key]:
                    res[key] = res[key]["id"]
            elif isinstance(Model._fields[key], fields.Selection):
                if isinstance(res[key], dict) and "value" in res[key]:
                    res[key] = res[key]["value"]
            elif isinstance(Model._fields[key], fields.Datetime):
                if res[key]:
                    res[key] = self.set_tz(res[key], 'Asia/Jakarta', 'UTC')
            elif isinstance(Model._fields[key], fields.One2many) or isinstance(Model._fields[key], fields.Many2many):
                res_list = []
                for res_line in res[key]:
                    if res_line.get('id'):
                        if res_line.get('delete'):
                            res_list += [
                                (2, int(res_line.get('id')))
                            ]
                        else:
                            res_list += [
                                (1, int(res_line.get('id')), {
                                 i: res_line[i] for i in res_line if i != 'id'})
                            ]
                    else:
                        res_list += [
                            (0, 0, res_line)
                        ]
                res[key] = res_list

        for key in list_deleted_key:
            del res[key]

        return res

    def filter_same_fields(self, model_name, model_id, update_values):
        Model = request.env[model_name].sudo()
        db_values = Model.browse(model_id).read([])
        list_deleted_key = []
        if db_values:
            db_values = db_values[0]
        for key in update_values:
            if (key in db_values and update_values[key] == db_values[key]):
                list_deleted_key.append(key)
        for key in list_deleted_key:
            del update_values[key]
        return update_values

    def error_response(self, status, error, error_descrip):
        return {
            'data': {
                'error': error,
                'error_descrip': error_descrip,
            },
            'code': status,
        }

    def check_session_id(func):

        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            if not request.session.uid:
                return self.error_response(401, 'Unauthorized', 'Unauthorized')
            else:
                request.uid = request.session.uid
            # The code, following the decorator
            return func(self, *args, **kwargs)

        return wrapper

    def validate_params(self, dictionary, params):
        res = True
        for param in params:
            if param not in dictionary:
                res = False
                return res
        return res

    def get_user_type(self, user):
        res = 'employee'
        # if user.has_group('api_rest_carfix.group_service_advisor'):
        #     res = 'service_advisor'
        # elif user.has_group('api_rest_carfix.group_foreman'):
        #     res = 'foreman'
        # elif user.has_group('api_rest_carfix.group_mechanic'):
        #     res = 'mechanic'
        # elif user.has_group('api_rest_carfix.group_call_center'):
        #     res = 'call_center'
        return res

    @http.route('/api/v1/login', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    def login(self, *args, **kwargs):
        jdata = request.jsonrequest
        res = {}
        User = request.env['res.users'].sudo()

        if jdata:
            login = jdata.get('login', False)
            password = jdata.get('password', False)
            db = request.db
            try:
                uid = request.session.authenticate(db, login, password)
            except Exception as e:
                return self.error_response(401, e, "Login or Password Incorrect")
            if uid:
                request.uid = uid

                user = User.search([('id', '=', uid)])
                partner = user.partner_id

                res = {
                    'id': uid,
                    'name': partner.name,
                    'username': login,
                    'session_id': request.session.sid,
                    'company_id': {
                        'id': user.company_id.id,
                        'name': user.company_id.name
                    }
                }
                # Check Groups
                res['user_type'] = self.get_user_type(user)
            else:
                return self.error_response(401, "Login or Password Incorrect", "Login or Password Incorrect")
        else:
            return self.error_response(401, "Login or Password Incorrect", "Login or Password Incorrect")
        return {
            'data': res,
            'code': 200,
        }

    @http.route('/api/v1/selections/<model_code>/<field>', methods=['GET'], type="json", auth="none", csrf=False, cors='*')
    def get_selections(self, model_code, field, *args, **kwargs):
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        Model = request.env[model].sudo()
        if isinstance(Model._fields[field], fields.Selection):
            res = []
            try:
                selection_dict = dict(
                    Model._fields[field].base_field.selection)
                for key in selection_dict:
                    res.append({
                        'name': selection_dict.get(key),
                        'value': key,
                        'subtitle': "",
                    })
            except Exception as e:
                res = []
        else:
            res = []
        return {
            'data': res,
            'code': 200,
        }

    @http.route([
        '/api/v1/read/<mode>/<model_code>',
        '/api/v1/read/<mode>/<model_code>/<int:model_id>'], methods=['GET'], type="json", auth="none", csrf=False, cors='*')
    @check_session_id
    def read_general(self, *args, mode, model_code, model_id=False, **kwargs):
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        company_id = user.company_id
        domain = []
        # domain += [('company_id', '=', company_id.id)]
        params = request.httprequest.args
        if mode == 'list' and not model_id:
            if 'domain_code' in params and self.get_domain_by_code(model, params['domain_code'], user):
                domain += self.get_domain_by_code(model,
                                                  params['domain_code'], user)
            res, meta = self.search_read_with_params(model, params, domain)
            for index, r in enumerate(res):
                res[index] = {
                    'id': r['id'],
                    'name': r['name']
                }
        elif mode == 'list-detail' and not model_id:
            if 'domain_code' in params and self.get_domain_by_code(model, params['domain_code'], user):
                domain += self.get_domain_by_code(model,
                                                  params['domain_code'], user)
            res, meta = self.search_read_with_params(model, params, domain)
            for r in res:
                r = self.prepare_fields_GET(model, r, [], True)
                r = self.prepare_specific_fields_list_GET(model, r)
        elif mode == 'detail' and model_id:
            res, meta = self.search_read_with_params(
                model, params, [('id', '=', model_id)])
            if res:
                res = res[0]
                res = self.prepare_fields_GET(model, res, [], False)
                res = self.prepare_specific_fields_detail_GET(model, res)
            return {
                'code': 200,
                'data': res,
            }
        else:
            return self.error_response(500, 'Error Input', 'Error Input')
        return {
            'data': res,
            'code': 200,
            'meta': meta
        }

    @http.route('/api/v1/create/<model_code>', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def create_general(self, model_code, *args, **kwargs):
        jdata = request.jsonrequest
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        company_id = user.company_id
        domain = []
        Model = request.env[model]
        jdata = self.prepare_fields_POST(model, jdata)
        jdata = self.prepare_specific_fields_create_POST(model, jdata)
        try:
            rec = Model.create(jdata)
            self.after_create(model, jdata, rec)
            rec = rec.read(self.fields_by_model[model])
            rec = self.prepare_fields_GET(model, rec[0], [], True)
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }

    @http.route('/api/v1/update/<model_code>/<int:model_id>', type="json", auth="none", methods=['PUT'], csrf=False, cors='*')
    @check_session_id
    def update_general(self, model_code, model_id, *args, **kwargs):
        jdata = request.jsonrequest
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        company_id = user.company_id
        domain = []
        Model = request.env[model]
        model_rec = Model.browse(model_id)
        if not model_rec:
            return self.error_response(500, 'Record Not Found', 'Record Not Found')
        jdata = self.prepare_fields_POST(model, jdata)
        jdata = self.prepare_specific_fields_write_POST(model, model_id, jdata)
        if jdata.get('id', False):
            del jdata['id']
        try:
            rec = model_rec.update(jdata)
            rec = model_rec.read(self.fields_by_model[model])
            rec = self.prepare_fields_GET(model, rec[0], [], True)
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }

    @http.route('/api/v1/delete/<model_code>/<int:model_id>', type="json", auth="none", methods=['DELETE'], csrf=False, cors='*')
    @check_session_id
    def delete_general(self, model_code, model_id, *args, **kwargs):
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        company_id = user.company_id
        domain = []
        Model = request.env[model]
        model_rec = Model.browse(model_id)
        if not model_rec:
            return self.error_response(500, 'Record Not Found', 'Record Not Found')
        try:
            rec = {
                'id': model_rec.id,
            }
            model_rec.unlink()
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }

    @http.route('/api/v1/financial/<report_type>', type="json", auth="none", methods=['GET'], csrf=False, cors='*')
    @check_session_id
    def get_financial_report(self, report_type, *args, **kwargs):
        if report_type == 'bs':
            report = request.env['account.financial.report'].search(
                [('name', 'ilike', 'Balance')], limit=1)
        elif report_type == 'pnl':
            report = request.env['account.financial.report'].search(
                [('name', 'ilike', 'Profit')], limit=1)
        else:
            return self.error_response(500, 'Report Not Found', 'Report Not Found')
        journals = request.env['account.journal'].search([])
        data = {
            'account_report_id': [report.id, report.name],
            'journal_ids': journals.ids,
            'comparison_context': {u'journal_ids': journals.ids, u'state': u'posted'},
            'date_from': False,
            'date_from_cmp': False,
            'date_to': False,
            'date_to_cmp': False,
            'debit_credit': False,
            'enable_filter': False,
            'filter_cmp': u'filter_no',
            'label_filter': False,
            'target_move': u'posted',
            'used_context': {u'date_from': False, u'date_to': False, u'journal_ids': journals.ids, u'lang': u'en_US', u'state': u'posted', u'strict_range': False},
        }
        account_lines = request.env[
            'report.account.report_financial'].get_account_lines(data)
        return {
            'data': account_lines,
            'code': 200,
        }

    @http.route('/api/v1/customers', type="json", auth="none", methods=['GET'], csrf=False, cors='*')
    @check_session_id
    def get_all_partner(self, *args, **kwargs):
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        params = request.httprequest.args
        res, meta = self.search_read_with_params(
            'res.partner', params, [('customer', '=', True)], sudo=True)
        for r in res:
            r = self.prepare_fields_GET('res.partner', r, [])
        return {
            'data': res,
            'code': 200,
            'meta': meta
        }

    @http.route('/api/v1/customers/<int:partner_id>', type="json", auth="none", methods=['GET'], csrf=False, cors='*')
    @check_session_id
    def get_partner(self, partner_id, *args, **kwargs):
        Partner = request.env['res.partner']
        res = request.env['res.partner'].sudo().search_read(
            [('id', '=', partner_id)], self.fields_by_model['res.partner'], limit=1)
        if res:
            res = res[0]
            res = self.prepare_fields_GET('res.partner', res, [])
        else:
            return self.error_response(500, 'Record Not Found', 'Record Not Found')
        return {
            'data': res,
            'code': 200,
        }

    @http.route('/api/v1/customers/new', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def create_partner(self, *args, **kwargs):
        jdata = request.jsonrequest
        Partner = request.env['res.partner'].sudo()
        jdata = self.prepare_fields_POST('res.partner', jdata)
        try:
            rec = Partner.create(jdata)
            rec = rec.read(self.fields_by_model['res.partner'])
            rec = self.prepare_fields_GET('res.partner', rec[0], [])
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }

    @http.route('/api/v1/customers/update/<int:partner_id>', methods=['POST'], type="json", auth="none", csrf=False, cors='*')
    @check_session_id
    def update_partner(self, partner_id, *args, **kwargs):
        jdata = request.jsonrequest
        Partner = request.env['res.partner'].sudo()
        partner = Partner.browse(partner_id)
        if not partner:
            return self.error_response(500, 'Record Not Found', 'Record Not Found')
        jdata = self.prepare_fields_POST('res.partner', jdata)
        try:
            partner.write(jdata)
            rec = partner.read(self.fields_by_model['res.partner'])
            rec = self.prepare_fields_GET('res.partner', rec[0], [])
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }


    @http.route('/api/v1/action/<action>/<model_code>/<int:model_id>', methods=['POST'], type="json", auth="none", csrf=False, cors='*')
    @check_session_id
    def action_general_post(self, action, model_code, model_id, *args, **kwargs):
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        company_id = user.company_id
        Model = request.env[model]
        model_rec = Model.browse(model_id)
        params = request.httprequest.args
        jdata = request.jsonrequest
        if not model_rec:
            return self.error_response(500, 'Record Not Found', 'Record Not Found')
        try:
            if model == 'project.task':
                Stage = request.env['project.task.type']
                Timesheet = request.env['account.analytic.line']
                Employee = request.env['hr.employee']
                if action == 'stop':
                    employee = Employee.search([('user_id', '=', user_id)])
                    if model_rec.state == 'start' and employee:
                        employee = employee[0]
                        timesheet = Timesheet.search(
                            [('employee_id', '=', employee.id), ('end_time', '=', False)], order='id desc')
                        if timesheet:
                            timesheet = timesheet[0]
                            if ('name' in jdata and jdata['name']):
                                timesheet.name = jdata['name']
                            if ('problem' in jdata and jdata['problem']):
                                timesheet.problem = jdata['problem']
                            if ('plan' in jdata and jdata['plan']):
                                timesheet.plan = jdata['plan']
                            timesheet.end_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                            duration = datetime.strptime(
                                timesheet.end_time, '%Y-%m-%d %H:%M:%S') - datetime.strptime(timesheet.start_time, '%Y-%m-%d %H:%M:%S')
                            timesheet.unit_amount = duration.total_seconds() / 3600
                        model_rec.state = 'stop'
            rec = {'id': model_rec.id}
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }

    @http.route('/api/v1/action/<action>/<model_code>/<int:model_id>', methods=['GET'], type="json", auth="none", csrf=False, cors='*')
    @check_session_id
    def action_general(self, action, model_code, model_id, *args, **kwargs):
        model = self.model_name_by_code.get(model_code)
        if not model:
            return self.error_response(400, 'Model Not Found', 'Model Not Found')
        user_id = request.session.uid
        user = request.env['res.users'].browse(user_id)
        company_id = user.company_id
        Model = request.env[model]
        model_rec = Model.browse(model_id)
        params = request.httprequest.args
        if not model_rec:
            return self.error_response(500, 'Record Not Found', 'Record Not Found')
        try:
            if model == 'project.task':
                Stage = request.env['project.task.type']
                Timesheet = request.env['account.analytic.line']
                Employee = request.env['hr.employee']
                if action == 'assigntome':
                    stage_progress = Stage.search(
                        [('name', 'like', 'On Progress')])
                    if stage_progress:
                        stage_progress = stage_progress[0]
                        model_rec.user_id = user_id
                        model_rec.stage_id = stage_progress.id
                if action == 'start':
                    stage_progress = Stage.search(
                        [('name', 'like', 'On Progress')])
                    if stage_progress:
                        stage_progress = stage_progress[0]
                        model_rec.stage_id = stage_progress.id
                    employee = Employee.search([('user_id', '=', user_id)])
                    if model_rec.state == 'stop' and employee:
                        employee = employee[0]
                        Timesheet.create({
                            'date': datetime.now().strftime('%Y-%m-%d'),
                            'employee_id': employee.id,
                            'user_id': user_id,
                            'name': '',
                            'project_id': model_rec.project_id.id,
                            'task_id': model_rec.id,
                            'unit_amount': 0,
                            'start_time': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                            'end_time': False,
                        })
                        model_rec.state = 'start'
                if action == 'stop':
                    employee = Employee.search([('user_id', '=', user_id)])
                    if model_rec.state == 'start' and employee:
                        employee = employee[0]
                        timesheet = Timesheet.search(
                            [('employee_id', '=', employee.id), ('end_time', '=', False)], order='id desc')
                        if timesheet:
                            timesheet = timesheet[0]
                            if ('notes' in params and params['notes']):
                                timesheet.name = params['notes']
                            timesheet.end_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                            duration = datetime.strptime(
                                timesheet.end_time, '%Y-%m-%d %H:%M:%S') - datetime.strptime(timesheet.start_time, '%Y-%m-%d %H:%M:%S')
                            timesheet.unit_amount = duration.total_seconds() / 3600
                        model_rec.state = 'stop'
                if action == 'done':
                    stage_done = Stage.search([('name', 'like', 'Test')])
                    if stage_done:
                        stage_done = stage_done[0]
                        model_rec.write({
                            'date_end': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                            'stage_id': stage_done.id,
                        })
                if action == 'reopen':
                    stage_progress = Stage.search(
                        [('name', 'like', 'On Progress')])
                    if stage_progress:
                        stage_progress = stage_progress[0]
                        model_rec.write({
                            'stage_id': stage_progress.id,
                        })
            if action == 'check_in':
                if model == 'hr.attendance':
                    employee = request.env['hr.employee'].search(
                        [('user_id', '=', user_id)])
                    if employee:
                        employee = employee[0]
                        Model.create({
                            'check_in': datetime.strftime('%Y-%m-%d %H:%M:%S'),
                            'employee_id': employee.id,
                        })
            if action == 'confirm':
                if model == 'sale.order':
                    model_rec.action_confirm()
                    invoice_ids = model_rec.action_invoice_create(final=True)
                    if invoice_ids:
                        invoice_id = invoice_ids[0]
                        request.env['account.invoice'].browse(
                            invoice_id).action_invoice_open()
                if model == 'purchase.order':
                    model_rec.button_confirm()
                    invoice = request.env['account.invoice'].create({
                        'partner_id': model_rec.partner_id.id,
                        'account_id': model_rec.partner_id.property_account_payable_id.id,
                        'type': 'in_invoice',
                        'purchase_id': model_rec.id})
                    invoice.purchase_order_change()
                    invoice.action_invoice_open()
                if model == 'account.invoice':
                    model_rec.action_invoice_open()
                if model == 'stock.picking':
                    model_rec.action_confirm()
            if action == 'check':
                if model == 'stock.picking':
                    model_rec.action_assign()
                if model == 'mrp.production':
                    model_rec.action_assign()
            if action == 'cancel':
                if model == 'sale.order':
                    model_rec.action_cancel()
                if model == 'purchase.order':
                    model_rec.button_cancel()
                if model == 'mrp.production':
                    model_rec.action_cancel()
                if model == 'stock.move':
                    model_rec._action_cancel()
            if action == 'validate':
                if model in ('purchase.order', 'sale.order'):
                    if model_rec.picking_ids:
                        picking = model_rec.picking_ids[0]
                        picking.action_confirm()
                        for move_line in picking.move_lines:
                            move_line._action_assign()
                            if move_line.state == 'assigned':
                                move_line.quantity_done = move_line.product_uom_qty
                                # move_line._action_done()
                        picking.action_done()
                if model == 'stock.picking':
                    for move_line in model_rec.move_lines:
                        move_line._action_assign()
                        if move_line.state == 'assigned':
                            # move_line.quantity_done = move_line.product_uom_qty
                            move_line._action_done()
            if action == 'process':
                if model == 'stock.move':
                    if model_rec.quantity_done > 0 and model_rec.quantity_done <= model_rec.product_uom_qty:
                        model_rec._action_done()
            if action == 'post':
                if model == 'account.move':
                    model_rec.post()
                if model == 'mrp.production':
                    model_rec.post_inventory()
            if action == 'pay':
                if model in ('purchase.order', 'sale.order'):
                    journal = request.env['account.journal'].search(
                        [('type', '=', 'bank')], limit=1)
                    if model_rec.invoice_ids:
                        invoice = model_rec.invoice_ids[0]
                        if invoice.state != 'open':
                            invoice.action_invoice_open()
                        invoice.pay_and_reconcile(pay_journal=journal)
                if model == 'account.invoice':
                    journal = request.env['account.journal'].search(
                        [('type', '=', 'bank')], limit=1)
                    model_rec.pay_and_reconcile(pay_journal=journal)
            if action == 'adjustqty':
                if model == 'product.product' and 'qty' in params:
                    model_rec.adjust_qty(float(params['qty']))
            rec = {'id': model_rec.id}
        except Exception as err:
            request.cr.rollback()
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': rec,
            'code': 200,
        }

    def after_create(self, model_name, jdata, rec):
        Model = request.env[model_name]
        Product = request.env['product.product']
        Bom = request.env['mrp.bom']
        if model_name == 'product.product':
            if 'manufacture' in jdata and jdata['manufacture']:
                bom = Bom.create({
                    'product_tmpl_id': rec.product_tmpl_id.id,
                    'product_qty': 1,
                    'type': 'normal',
                })

    def prepare_specific_fields_write_POST(self, model_name, id, res):
        Model = request.env[model_name]
        Product = request.env['product.product']
        Category = request.env['product.category']
        Picking = request.env['stock.picking']
        Tax = request.env['account.tax']
        Bom = request.env['mrp.bom']

        model_rec = Model.browse(id)

        if model_name == 'product.product':
            if 'purchase_price' in res and model_rec.standard_price == 0:
                res['standard_price'] = res['purchase_price']
        if model_name == 'purchase.order.line':
            if 'tax_ok' in res and res['tax_ok']:
                purchase_tax = Tax.search(
                    [('type_tax_use', '=', 'purchase')], limit=1)
                if purchase_tax:
                    res['taxes_id'] = [(4, purchase_tax.id)]
            elif 'tax_ok' in res and not res['tax_ok']:
                res['taxes_id'] = [(5, 0, 0)]
            if 'tax_ok' in res:
                del res['tax_ok']
        if model_name == 'sale.order.line':
            if 'tax_ok' in res and res['tax_ok']:
                sale_tax = Tax.search([('type_tax_use', '=', 'sale')], limit=1)
                if sale_tax:
                    res['tax_id'] = [(4, sale_tax.id)]
            elif 'tax_ok' in res and not res['tax_ok']:
                res['tax_id'] = [(5, 0, 0)]
            if 'tax_ok' in res:
                del res['tax_ok']
        return res

    def prepare_specific_fields_create_POST(self, model_name, res):
        Model = request.env[model_name]
        Product = request.env['product.product']
        Category = request.env['product.category']
        Picking = request.env['stock.picking']
        Tax = request.env['account.tax']
        Bom = request.env['mrp.bom']
        Stage = request.env['project.task.type']
        if model_name == 'project.task':
            if 'stage_id' not in res:
                stage = Stage.search([('name', 'like', 'To Do')])
                if stage:
                    res['stage_id'] = stage[0].id
        if model_name == 'mrp.production':
            if 'product_uom_id' not in res:
                res['product_uom_id'] = Product.browse(
                    res['product_id']).uom_id.id
            if 'bom_id' not in res:
                product_tmpl = Product.browse(
                    res['product_id']).product_tmpl_id
                if product_tmpl.bom_ids:
                    res['bom_id'] = product_tmpl.bom_ids[0].id
        if model_name == 'product.product':
            res['purchase_method'] = 'purchase'
            res['type'] = 'product'
            categ = Category.search([], limit=1)
            if categ:
                res['categ_id'] = categ.id
            res['standard_price'] = res['purchase_price']
            if 'uom_po_id' not in res and 'uom_id' in res:
                res['uom_po_id'] = res['uom_id']
        if model_name == 'purchase.order.line':
            if 'date_planned' not in res:
                res['date_planned'] = datetime.today().strftime('%Y-%m-%d')
            if 'product_uom' not in res and 'product_id' in res:
                res['product_uom'] = Product.browse(
                    res['product_id']).uom_id.id
            purchase_tax = Tax.search(
                [('type_tax_use', '=', 'purchase')], limit=1)
            if purchase_tax:
                res['taxes_id'] = [(4, purchase_tax.id)]
        if model_name == 'sale.order.line':
            if 'product_uom' not in res and 'product_id' in res:
                res['product_uom'] = Product.browse(
                    res['product_id']).uom_id.id
            sale_tax = Tax.search([('type_tax_use', '=', 'sale')], limit=1)
            if sale_tax:
                res['tax_id'] = [(4, sale_tax.id)]
        if model_name == 'stock.move':
            if 'scheduled_date' not in res:
                res['scheduled_date'] = datetime.today().strftime('%Y-%m-%d')
            if 'product_uom' not in res and 'product_id' in res:
                res['product_uom'] = Product.browse(
                    res['product_id']).uom_id.id
            if 'name' not in res and 'product_id' in res:
                res['name'] = Product.browse(res['product_id']).name
            if 'location_id' not in res and 'picking_id' in res:
                res['location_id'] = Picking.browse(
                    res['picking_id']).location_id.id
            if 'location_dest_id' not in res and 'picking_id' in res:
                res['location_dest_id'] = Picking.browse(
                    res['picking_id']).location_dest_id.id
        return res

    def prepare_specific_fields_list_GET(self, model_name, res):
        Model = request.env[model_name]
        model_rec = Model.browse(res['id'])
        if model_name == 'project.project':
            res['has_start_task'] = False
            res['my_project'] = False
            if ('user_id' in res and 'id' in res['user_id'] and request.env.user.id == res['user_id']['id']):
                res['my_project'] = True
            for index, task in enumerate(res['tasks']):
                Timesheet = request.env['account.analytic.line']
                timesheet = Timesheet.search(
                    [('task_id', '=', task['id'])], order='id desc', limit=1)
                if timesheet:
                    timesheet = timesheet[0]
                    res['tasks'][index]['timesheet_name'] = timesheet['name']
                    res['tasks'][index][
                        'timesheet_problem'] = timesheet['problem']
                    res['tasks'][index]['timesheet_plan'] = timesheet['plan']
                res['tasks'][index]['my_task'] = False
                if ('user_id' in task and 'id' in task['user_id'] and request.env.user.id == task['user_id']['id']):
                    res['tasks'][index]['my_task'] = True
                    if ('user_id' in task and 'id' in task['user_id'] and request.env.user.id == task['user_id']['id']) and 'state' in task and task['state']['value'] == 'start':
                        res['has_start_task'] = True
            # Reorder Task By Deadline & Hide Done Task
            stage_done = request.env['project.task.type'].search(
                [('name', 'like', 'Done')])
            if stage_done:
                stage_done = stage_done[0]
                tasks = res['tasks']
                res['tasks'] = []
                for task in tasks:
                    task['over_deadline'] = False
                    if task['date_deadline']:
                        diff = datetime.now() - \
                            datetime.strptime(
                                task['date_deadline'], '%Y-%m-%d')
                        if diff.days >= 0:
                            task['over_deadline'] = True
                    # and diff.days > 7:
                    if task['stage_id'] and task['stage_id']['id'] == stage_done.id:
                        continue
                    res['tasks'].append(task)
            res['tasks'] = sorted(res['tasks'], key=lambda i: i[
                                  'stage_id']['id'], reverse=True)

        if model_name == 'hr.attendance':
            if res['check_in'] and not res['check_out']:
                diff = datetime.now() - \
                    datetime.strptime(res['check_in'], '%Y-%m-%d %H:%M:%S')
                res['worked_hours'] = math.floor(
                    diff.total_seconds() / 360) / 10
        if model_name == 'product.product':
            if model_rec.bom_id:
                res['manufacture'] = True
        if model_name == 'account.move':
            res['custom_state'] = 'Draft'
            if model_rec.state == 'posted':
                res['custom_state'] = 'Posted'
            for index, r in enumerate(res['line_ids']):
                if r['id'] == model_rec.line_ids[index].id:
                    res['line_ids'][index]['account_id'][
                        'display_name'] = model_rec.line_ids[index].account_id.display_name
        if model_name == 'purchase.order':
            res['custom_state'] = 'Draft'
            if model_rec.state == 'purchase':
                if(model_rec.picking_ids and model_rec.picking_ids[0].state != 'done'):
                    res['custom_state'] = 'Barang Masuk'
                elif (model_rec.invoice_ids and model_rec.invoice_ids[0].state != 'paid'):
                    res['custom_state'] = 'Pembayaran'
                elif(model_rec.invoice_ids and model_rec.invoice_ids[0].state == 'paid') and (model_rec.picking_ids and model_rec.picking_ids[0].state == 'done'):
                    res['custom_state'] = 'Selesai'
                elif(not model_rec.picking_ids and not model_rec.invoice_ids):
                    res['custom_state'] = 'Dibatalkan'
            elif model_rec.state == 'cancel':
                res['custom_state'] = 'Dibatalkan'
            for index, r in enumerate(res['order_line']):
                if r['id'] == model_rec.order_line[index].id:
                    if model_rec.order_line[index].taxes_id:
                        res['order_line'][index]['tax_ok'] = True
                    else:
                        res['order_line'][index]['tax_ok'] = False
        if model_name == 'sale.order':
            res['custom_state'] = 'Draft'
            if model_rec.state == 'sale':
                if(model_rec.picking_ids and model_rec.picking_ids[0].state != 'done'):
                    res['custom_state'] = 'Barang Keluar'
                elif (model_rec.invoice_ids and model_rec.invoice_ids[0].state != 'paid'):
                    res['custom_state'] = 'Pembayaran'
                elif(model_rec.invoice_ids and model_rec.invoice_ids[0].state == 'paid') and (model_rec.picking_ids and model_rec.picking_ids[0].state == 'done'):
                    res['custom_state'] = 'Selesai'
                elif(not model_rec.picking_ids and not model_rec.invoice_ids):
                    res['custom_state'] = 'Dibatalkan'
            elif model_rec.state == 'cancel':
                res['custom_state'] = 'Dibatalkan'
            for index, r in enumerate(res['order_line']):
                if r['id'] == model_rec.order_line[index].id:
                    if model_rec.order_line[index].tax_id:
                        res['order_line'][index]['tax_ok'] = True
                    else:
                        res['order_line'][index]['tax_ok'] = False
        return res

    def prepare_specific_fields_detail_GET(self, model_name, res):
        res = self.prepare_specific_fields_list_GET(model_name, res)
        Model = request.env[model_name]
        model_rec = Model.browse(res['id'])
        if model_name == 'purchase.order.line':
            if model_rec.taxes_id:
                res['tax_ok'] = True
            else:
                res['tax_ok'] = False
        if model_name == 'sale.order.line':
            if model_rec.tax_id:
                res['tax_ok'] = True
            else:
                res['tax_ok'] = False
        if model_name == 'sale.order':
            if model_rec.invoice_ids:
                res['left_action'] = {
                    'value': model_rec.invoice_ids[0].number or 'Draft',
                    'label': 'Tagihan',
                    'id': model_rec.invoice_ids[0].id,
                }
            if model_rec.picking_ids:
                res['right_action'] = {
                    'value': model_rec.picking_ids[0].name or 'Draft',
                    'label': 'Pengiriman',
                    'id': model_rec.picking_ids[0].id,
                }
        if model_name == 'purchase.order':
            if model_rec.invoice_ids:
                res['left_action'] = {
                    'value': model_rec.invoice_ids[0].number or 'Draft',
                    'label': 'Tagihan',
                    'id': model_rec.invoice_ids[0].id,
                }
            if model_rec.picking_ids:
                res['right_action'] = {
                    'value': model_rec.picking_ids[0].name or 'Draft',
                    'label': 'Penerimaan',
                    'id': model_rec.picking_ids[0].id,
                }
        return res

    def get_domain_by_code(self, model_name, domain_code, user):
        domain = False
        if model_name == 'account.account':
            if domain_code == 'income':
                domain = [('user_type_id.name', 'in',
                           ('Income', 'Other Income'))]
            if domain_code == 'expense':
                domain = [('user_type_id.name', 'in',
                           ('Expenses', 'Cost of Revenue'))]
        if model_name == 'account.journal':
            if domain_code == 'cashbank':
                domain = [('type', 'in', ('cash', 'bank'))]
        if model_name == 'account.analytic.line':
            if domain_code == 'timesheet':
                domain = [('project_id', '!=', False),
                          ('user_id', '=', user.id)]
        if model_name == 'project.project':
            if domain_code == 'project':
                domain = ['|', ('member_ids', 'in', user.id),
                          ('user_id', '=', user.id)]
        return domain

    @http.route('/api/v1/sale/summary', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_sale_summary(self, *args, **kwargs):
        jdata = request.jsonrequest
        if jdata:
            mode = jdata.get('mode', False)
        try:
            vals = SaleSummary().get_box_data(request, mode)
        except Exception as err:
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': vals,
            'code': 200,
        }

    @http.route('/api/v1/sale/summary/list', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    @check_session_id
    def get_sale_summary_list_page(self, *args, **kwargs):
        jdata = request.jsonrequest
        if jdata:
            mode = jdata.get('mode', False)
            report = jdata.get('report', False)
        try:
            vals = SaleSummary().get_list_page(request, mode, report)
        except Exception as err:
            return self.error_response(500, 'Server Error', '%s' % err.args[0])
        return {
            'data': vals,
            'code': 200,
        }
