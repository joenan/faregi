# -*- coding: utf-8 -*-
import logging
import werkzeug

from odoo import http, _
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.auth_signup.controllers.main import ensure_db, AuthSignupHome
from odoo.addons.base_setup.controllers.main import BaseSetup
from odoo.exceptions import UserError
from odoo.http import request
_logger = logging.getLogger(__name__)


class SignupRest(AuthSignupHome):

    def error_response(self, status, error, error_descrip):
        return {
            'data': {
                'error': error,
                'error_descrip': error_descrip,
            },
            'code': status,
        }

    @http.route('/api/v1/login', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    def login(self, *args, **kwargs):
        jdata = request.jsonrequest
        res = {}
        User = request.env['res.users'].sudo()
        if jdata:
            login = jdata.get('login', False)
            password = jdata.get('password', False)
            db = request.db
            try:
                uid = request.session.authenticate(db, login, password)
            except Exception as e:
                return self.error_response(401, "Login Failed", e)
            if uid:
                request.uid = uid
                user = User.search([('id', '=', uid)])
                partner = user.partner_id
                res = {
                    'id': uid,
                    'name': partner.name,
                    'username': login,
                    'session_id': request.session.sid,
                    'company_id': {
                        'id': user.company_id.id,
                        'name': user.company_id.name
                    }
                }
            else:
                return self.error_response(401, "Login Failed", "Login or Password Incorrect")
        else:
            return self.error_response(401, "Login Failed", "Login or Password Incorrect")
        return {
            'data': res,
            'code': 200,
        }

    @http.route('/api/v1/signup', type="json", auth="none", methods=['POST'], csrf=False, cors='*')
    def signup(self, *args, **kwargs):
        jdata = request.jsonrequest
        if jdata:
            # confirm password
            if jdata.get('password', False) and jdata.get('confirm_password', False):
                if jdata.get('password', False) != jdata.get('confirm_password', False):
                    return self.error_response(401, "Signup failed", 'Please re-confirm password')
            else:
                return self.error_response(401, "Signup failed", 'Please fill in Password and Confirm Password')
            qcontext = {
                'login': jdata.get('login', False),
                'name': jdata.get('name', False),
                'password': jdata.get('password', False),
                'confirm_password': jdata.get('confirm_password', False),
                'active': True,
            }
            if 'error' not in qcontext and request.httprequest.method == 'POST':
                try:
                    template_user = request.env.ref(
                        'base.template_portal_user_id').sudo()
                    del qcontext['confirm_password']
                    user = template_user.copy(qcontext)
                    # Send an account creation confirmation email
                    user_sudo = user.sudo()
                    template = request.env.ref(
                        'auth_signup.mail_template_user_signup_account_created', raise_if_not_found=False)
                    if user_sudo and template:
                        template.sudo().with_context(
                            lang=user_sudo.lang,
                            auth_login=werkzeug.url_encode(
                                {'auth_login': user_sudo.email}),
                        ).send_mail(user_sudo.id, force_send=True)
                    request.env.cr.commit()
                    return self.login(*args, **kwargs)
                except UserError as e:
                    qcontext['error'] = e.name or e.value
                except (SignupError, AssertionError) as e:
                    if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                        qcontext["error"] = _(
                            "Another user is already registered using this email address.")
                    else:
                        _logger.error("%s", e)
                        qcontext['error'] = _(
                            "Could not create a new account.")
            return self.error_response(401, "Signup failed", qcontext['error'])
        else:
            return self.error_response(401, "Signup failed", "Please fill the data required")
