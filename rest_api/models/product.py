from odoo import models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def api_get_product(self, product_id=False, limit=False, offset=False, order=None):
        field_list = ['name', 'list_price']
        if product_id:
            result = self.browse(product_id).read(field_list)
            count = 1
        else:
            search_list = [['sale_ok', '=', True]]
            product_ids = self.search(
                search_list, limit=limit, offset=offset, order=order)
            result = product_ids.read(field_list)
            count = len(result)
        return result, count


class ProductCategory(models.Model):
    _inherit = 'product.category'

    def api_get_product(self, limit=False, offset=False, order=None):
        field_list = ['name']
        search_list = [['sale_ok', '=', True]]
        category_ids = self.search(
            search_list, limit=limit, offset=offset, order=order)
        result = category_ids.read(field_list)
        count = len(result)
        return result, count
