from odoo import models, fields, api, _
from odoo.tools.float_utils import float_compare, float_round, float_is_zero
from odoo.exceptions import UserError
import re
import requests
import json

URL = 'https://fcm.googleapis.com/fcm/send'
HEADERS = {
    'Authorization': '',
    'Content-Type': 'application/json'
}

class Company(models.Model):
    _inherit = 'res.company'

    warkana_firebase_authorization = fields.Char('WArkana Firebase Authorization')
    warkana_firebase_token = fields.Char('WArkana Firebase Token')
    wa_project_user_ids = fields.Many2many(
        comodel_name='res.users', 
        string='Project Users to Notify'
    )

    @api.model
    def reminder_start_task(self):
        company = self.env.user.company_id
        for user in company.wa_project_user_ids:
            if not user.partner_id or not user.partner_id.phone:
                continue
            try:
                phone = re.sub('\D', '', user.partner_id.phone)
                if phone[0] == '0':
                    phone = '62' + phone[1:]
                
                headers = HEADERS
                headers['Authorization'] = company.warkana_firebase_authorization
                payload = {
                    'to': company.warkana_firebase_token,
                    'data': {
                        'phone': phone,
                        'body': '%s jangan lupa start task di Arkana Timesheet' % (user.partner_id.name)
                    }
                }
                r = requests.post(URL, headers=headers, data=json.dumps(payload))
            except requests.exceptions.RequestException as e:
                continue

    @api.model
    def reminder_stop_task(self):
        company = self.env.user.company_id
        started_tasks = self.env['project.task'].search([('state', '=', 'start'), ('user_id', 'in', company.wa_project_user_ids.ids)])
        for task in started_tasks:
            try:
                user = task.user_id
                if not user.partner_id or not user.partner_id.phone:
                    continue
                phone = re.sub('\D', '', user.partner_id.phone)
                if phone[0] == '0':
                    phone = '62' + phone[1:]
                
                headers = HEADERS
                headers['Authorization'] = company.warkana_firebase_authorization
                payload = {
                    'to': company.warkana_firebase_token,
                    'data': {
                        'phone': phone,
                        'body': '%s jangan lupa stop task %s di Arkana Timesheet' % (user.partner_id.name, task.name)
                    }
                }
                r = requests.post(URL, headers=headers, data=json.dumps(payload))
                return r.json()
            except requests.exceptions.RequestException as e:
                continue


class ProjectProject(models.Model):
    _inherit = 'project.project'

    wa_to_pm = fields.Boolean('Notify PM')
    wa_new_task = fields.Boolean('Notify New Task')

class ProjectTask(models.Model):
    _inherit = 'project.task'

    @api.model
    def create(self, vals):
        res = super(ProjectTask, self).create(vals)
        company = self.env.user.company_id
        if company.warkana_firebase_token and company.warkana_firebase_authorization \
            and self.user_id and self.user_id.partner_id.phone and self.project_id and self.project_id.wa_new_task:
            try:
                phone = re.sub('\D', '', self.user_id.partner_id.phone)
                if phone[0] == '0':
                    phone = '62' + phone[1:]
                
                headers = HEADERS
                headers['Authorization'] = company.warkana_firebase_authorization
                payload = {
                    'to': company.warkana_firebase_token,
                    'data': {
                        'phone': phone,
                        'body': '%s, ada task baru : %s' % (self.user_id.partner_id.name, self.name)
                    }
                }
                r = requests.post(URL, headers=headers, data=json.dumps(payload))
            except requests.exceptions.RequestException as e:
                res = res
        return res

    @api.multi
    def write(self, vals):
        res = super(ProjectTask, self).write(vals)
        company = self.env.user.company_id
        if company.warkana_firebase_token and company.warkana_firebase_authorization \
            and self.user_id and self.user_id.partner_id.phone \
            and 'user_id' in vals and self.project_id and self.project_id.wa_new_task:
            try:
                phone = re.sub('\D', '', self.user_id.partner_id.phone)
                if phone[0] == '0':
                    phone = '62' + phone[1:]
                
                headers = HEADERS
                headers['Authorization'] = company.warkana_firebase_authorization
                payload = {
                    'to': company.warkana_firebase_token,
                    'data': {
                        'phone': phone,
                        'body': '%s, ada task baru : %s' % (self.user_id.partner_id.name, self.name)
                    }
                }
                r = requests.post(URL, headers=headers, data=json.dumps(payload))
            except requests.exceptions.RequestException as e:
                res = res

        if company.warkana_firebase_token and company.warkana_firebase_authorization and self.project_id.wa_to_pm \
            and self.user_id and self.project_id.user_id and self.project_id.user_id.partner_id.phone \
            and 'state' in vals and self.timesheet_ids:
            try:
                timesheet = sorted(self.timesheet_ids, key=lambda i: i.id, reverse=True)
                timesheet = timesheet[0]
                phone = re.sub('\D', '', self.project_id.user_id.partner_id.phone)
                if phone[0] == '0':
                    phone = '62' + phone[1:]
                
                headers = HEADERS
                headers['Authorization'] = company.warkana_firebase_authorization
                if vals['state'] == 'start':
                    payload = {
                        'to': company.warkana_firebase_token,
                        'data': {
                            'phone': phone,
                            'body': '%s mulai mengerjakan task %s' % (self.user_id.partner_id.name, self.name)
                        }
                    }
                if vals['state'] == 'stop':
                    payload = {
                        'to': company.warkana_firebase_token,
                        'data': {
                            'phone': phone,
                            'body': '%s berhenti mengerjakan task %s. Progress : %s. Kendala : %s. Rencana : %s.' % (self.user_id.partner_id.name, self.name, timesheet.name or '', timesheet.problem or '', timesheet.plan or '')
                        }
                    }
                r = requests.post(URL, headers=headers, data=json.dumps(payload))
            except requests.exceptions.RequestException as e:
                res = res
            
        return res
