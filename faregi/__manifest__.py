# Copyright 2015-TODAY Eficent
# - Jordi Ballester Alomar
# Copyright 2015-TODAY Serpent Consulting Services Pvt. Ltd. - Sudhir Arya
# License: LGPL-3 or later (https://www.gnu.org/licenses/lgpl.html).
{
    "name": "Fare.gi",
    "summary": "Custom Module for Faregi App",
    "version": "13.1",
    "author": "Joenan, Faalih",
    "website": "https://arkana.co.id/",
    "category": "Generic",
    "depends": ["base"],
    "license": "LGPL-3",
    "data": [
        "security/faregi_security.xml",
        "security/ir.model.access.csv",
        "data/menu_data.xml",
        "view/itenerary_view.xml",
        "view/flight_view.xml",
        "view/explore_view.xml",
        "view/todo_view.xml",
    ],
    'demo': [
    ],
    'application': True,
}
