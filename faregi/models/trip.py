# -*- coding: utf-8 -*-

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import AccessError, UserError, ValidationError


class TourTrip(models.Model):
    _name = 'tour.trip'
    _description = 'Trip'
    _order = 'date_from desc'

    name = fields.Char('Name', required=True)
    image = fields.Image(string='Image')
    description = fields.Text('Description')
    duration = fields.Char('Duration', compute='_duration')
    date_from = fields.Date('Date From', required=True)
    date_to = fields.Date('Date To', required=True)
    city_from = fields.Char('City From', required=True)
    city_to = fields.Char('City To', required=True)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda r: r.env.company.currency_id.id, required=True)
    price = fields.Monetary('Total Price', required=True)
    transport = fields.Selection([
        ('Plane', 'Plane'),
        ('Train', 'Train'),
        ('Car', 'Car'),
        ('None', 'None'),
    ], string='Transportation', required=True)
    itenerary_ids = fields.One2many(
        'tour.trip.itenerary', 'trip_id', string='Itenerary')
    flight_ids = fields.One2many(
        'tour.trip.flight', 'trip_id', string='Flights')
    train_ids = fields.One2many(
        'tour.trip.train', 'trip_id', string='Trains')
    car_ids = fields.One2many(
        'tour.trip.car', 'trip_id', string='Cars')
    hotel_ids = fields.One2many(
        'tour.trip.hotel', 'trip_id', string='Hotels')
    book_user_ids = fields.Many2many('res.users', string='User Booked')
    booked = fields.Boolean(string='Booked', compute='_booked')

    def _booked(self):
        for trip in self:
            if self.env.user in trip.book_user_ids:
                trip.booked = True
            else:
                trip.booked = False

    @api.depends('date_from', 'date_to')
    def _duration(self):
        for trip in self:
            if trip.date_from and trip.date_to:
                if trip.date_to < trip.date_from:
                    raise UserError(
                        _('Date To must be after or equal to Date From'))
                diff = trip.date_to - trip.date_from
                days = diff.days + 1
                nights = diff.days
                trip.duration = str(days) + ' Days ' + \
                    str(nights) + ' Nights'
            else:
                trip.duration = ''


class TourTripItenerary(models.Model):
    _name = 'tour.trip.itenerary'
    _description = 'Trip Itenerary'
    _order = 'sequence'

    name = fields.Char('Name', required=True)
    sequence = fields.Integer('Sequence', default=10)
    image = fields.Image(string='Image')
    date = fields.Date('Date', required=True)
    duration = fields.Float('Duration', digits=(12, 1), required=True)
    hour = fields.Float('Time', digits=(12, 2), required=True)
    end = fields.Boolean('End')
    trip_id = fields.Many2one(
        'tour.trip', string='Trip', required=True, ondelete='cascade', index=True)
    todo_id = fields.Many2one(
        'tour.todo', string='Things to Do')


class TourTripFlight(models.Model):
    _name = 'tour.trip.flight'
    _description = 'Flights'
    _order = 'departure_datetime'

    code = fields.Char(string='Flight Code', required=True)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda r: r.env.company.currency_id.id, required=True)
    price = fields.Monetary(string='Price', required=True)
    airline_id = fields.Many2one(
        'flight.airline', string='Airline', required=True)
    trip_id = fields.Many2one(
        'tour.trip', string='Trip', required=True)
    class_type = fields.Selection([
        ('Economy', 'Economy'),
        ('Business', 'Business'),
    ], string='Class', required=True)
    departure_city = fields.Char(string='Departure City', required=True)
    arrival_city = fields.Char(string='Arrival City', required=True)
    departure_datetime = fields.Datetime(
        string='Departure Time', required=True)
    arrival_datetime = fields.Datetime(string='Arrival Time', required=True)
    round_trip = fields.Boolean('Round-trip')
    return_departure_datetime = fields.Datetime(
        string='Ret. Departure Time')
    return_arrival_datetime = fields.Datetime(
        string='Ret. Arrival Time')
    book_user_ids = fields.Many2many('res.users', string='User Booked')
    booked = fields.Boolean(string='Booked', compute='_booked')

    def _booked(self):
        for trip in self:
            if self.env.user in trip.book_user_ids:
                trip.booked = True
            else:
                trip.booked = False


class FlightAirline(models.Model):
    _name = 'flight.airline'
    _description = 'Airline'

    name = fields.Char(string='Name', required=True)
    logo = fields.Image(string='Logo')


class TourTripTrain(models.Model):
    _name = 'tour.trip.train'
    _description = 'Train'
    _order = 'departure_datetime'

    train = fields.Char(string='Train', required=True)
    code = fields.Char(string='Train Code', required=True)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda r: r.env.company.currency_id.id, required=True)
    price = fields.Monetary(string='Price', required=True)
    trip_id = fields.Many2one(
        'tour.trip', string='Trip', required=True)
    class_type = fields.Selection([
        ('Economy', 'Economy'),
        ('Business', 'Business'),
    ], string='Class', required=True)
    departure_city = fields.Char(string='Departure City', required=True)
    arrival_city = fields.Char(string='Arrival City', required=True)
    departure_datetime = fields.Datetime(
        string='Departure Time', required=True)
    arrival_datetime = fields.Datetime(string='Arrival Time', required=True)
    round_trip = fields.Boolean('Round-trip')
    return_departure_datetime = fields.Datetime(
        string='Ret. Departure Time')
    return_arrival_datetime = fields.Datetime(
        string='Ret. Arrival Time')
    book_user_ids = fields.Many2many('res.users', string='User Booked')
    booked = fields.Boolean(string='Booked', compute='_booked')

    def _booked(self):
        for trip in self:
            if self.env.user in trip.book_user_ids:
                trip.booked = True
            else:
                trip.booked = False


class TourTripCar(models.Model):
    _name = 'tour.trip.car'
    _description = 'Car Rent'
    _order = 'date'

    name = fields.Char(string='Name', required=True)
    transmission = fields.Selection([
        ('Automatic', 'Automatic'),
        ('Manual', 'Manual'),
    ], string='Transmission', required=True)
    image = fields.Image(string='Image')
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda r: r.env.company.currency_id.id, required=True)
    price = fields.Monetary(string='Rent/Days', required=True)
    trip_id = fields.Many2one(
        'tour.trip', string='Trip', required=True)
    passenger = fields.Integer(string='Passenger', required=True)
    driver = fields.Boolean('Driver')
    location = fields.Char('Location')
    date = fields.Date(string='Date')
    book_user_ids = fields.Many2many('res.users', string='User Booked')
    booked = fields.Boolean(string='Booked', compute='_booked')

    def _booked(self):
        for trip in self:
            if self.env.user in trip.book_user_ids:
                trip.booked = True
            else:
                trip.booked = False


class TourTripHotel(models.Model):
    _name = 'tour.trip.hotel'
    _description = 'Hotel'

    name = fields.Char(string='Name', required=True)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda r: r.env.company.currency_id.id, required=True)
    price = fields.Monetary(string='Price/Night', required=True)
    trip_id = fields.Many2one(
        'tour.trip', string='Trip', required=True)
    location = fields.Char(string='Location', required=True)
    description = fields.Text('Description')
    rating = fields.Integer('Rating', help="Put 1 to 5")
    book_user_ids = fields.Many2many('res.users', string='User Booked')
    booked = fields.Boolean(string='Booked', compute='_booked')
    image_ids = fields.One2many('hotel.image', 'hotel_id', string='Images')

    def _booked(self):
        for trip in self:
            if self.env.user in trip.book_user_ids:
                trip.booked = True
            else:
                trip.booked = False

    @api.onchange('rating')
    def on_change_rating(self):
        if self.rating > 5 or self.rating < 0:
            raise ValidationError(_('Enter Rating value between 1-5.'))


class HotelImage(models.Model):
    _name = 'hotel.image'
    _description = 'Hotel Image'
    _order = 'sequence'

    name = fields.Char(string='Name', required=True)
    image = fields.Image(string='Image', required=True)
    sequence = fields.Integer(string='Sequence', default=10)
    primary = fields.Boolean(
        string='Primary', compute='_set_primary', store=True)
    hotel_id = fields.Many2one(
        'tour.trip.hotel', string='Hotel', required=True, ondelete='cascade')

    @api.depends('sequence')
    def _set_primary(self):
        for image in self:
            image.primary = False
            image_seq_list = self.search(
                [], order='sequence', limit=1).mapped('sequence')
            if image_seq_list:
                if image.sequence == image_seq_list[0]:
                    image.primary = True


class TourExplore(models.Model):
    _name = 'tour.explore'
    _description = 'Explore'
    _order = 'name'

    name = fields.Char('Name', required=True)
    image = fields.Image(string='Image')
    description = fields.Text('Description')
    todo_ids = fields.Many2many('tour.todo', string='Things to Do')


class TourTodo(models.Model):
    _name = 'tour.todo'
    _description = 'To do'
    _order = 'name'

    name = fields.Char('Name', required=True)
    image = fields.Image(string='Image')
    description = fields.Text('Description')
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda r: r.env.company.currency_id.id, required=True)
    price = fields.Monetary(string='Price', required=True)
    book_user_ids = fields.Many2many('res.users', string='User Booked')
    booked = fields.Boolean(string='Booked', compute='_booked')

    def _booked(self):
        for trip in self:
            if self.env.user in trip.book_user_ids:
                trip.booked = True
            else:
                trip.booked = False
